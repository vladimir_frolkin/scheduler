package net.jet800.scheduler;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledFuture;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jet800
 */
public class SchedulerTest {

    /**
     * Test of schedule method, of class Scheduler.
     */
    @Test
    public void testSchedule() throws InterruptedException, ExecutionException {
        System.out.println("schedule 1 sec diff");
        Scheduler instance = new Scheduler();
        ZonedDateTime start = ZonedDateTime.now();
        ScheduledFuture<ZonedDateTime> future = instance.schedule(ZonedDateTime.now().plus(1, ChronoUnit.SECONDS), new CallMeLater());
        ZonedDateTime result = future.get();
        long diff = start.until(result, ChronoUnit.SECONDS);
        assertTrue("difference was " + diff, diff == 1);

        System.out.println("schedule 100 millis diff");
        start = ZonedDateTime.now();
        future = instance.schedule(ZonedDateTime.now().plus(100, ChronoUnit.MILLIS), new CallMeLater());
        result = future.get();
        diff = start.until(result, ChronoUnit.MILLIS);
        assertTrue("difference was " + diff, diff - 100<=10); //allow up to 10 ms error

    }

    /**
     * Simple test callable that returns when it was actually executed
     */
    private class CallMeLater implements Callable<ZonedDateTime> {

        @Override
        public ZonedDateTime call() throws Exception {
            return ZonedDateTime.now();
        }

    }

}
