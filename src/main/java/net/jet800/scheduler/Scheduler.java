package net.jet800.scheduler;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.Callable;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

/**
 * This class allows of scheduling any Callable to be run at specified Date and time.
 * Implementation is based on {@link ScheduledExecutorService}
 * 
 * @author jet800
 */
public class Scheduler {
    
    private final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
    
    /**
     * Creates and executes a ScheduledFuture that becomes enabled at the
     * given date.
     *
     * @param callable the function to execute
     * @param dateTime the date at execution will be scheduled
     * @param <T> the type of the callable's result
     * @return a ScheduledFuture that can be used to extract result or cancel
     */
    public <T> ScheduledFuture<T> schedule(ZonedDateTime dateTime, Callable<T> callable) {
        long delay = ZonedDateTime.now().until(dateTime, ChronoUnit.NANOS);
        return scheduledExecutorService.schedule(callable, delay, TimeUnit.NANOSECONDS);
    }
    
}
